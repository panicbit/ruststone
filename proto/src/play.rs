use failure::Error;

pub mod server;
pub mod client;

#[derive(Copy,Clone,Debug)]
pub enum GameMode {
    Survival = 0,
    Creative = 1,
    Adventure = 2,
    Spectator = 3,
}

#[derive(Copy,Clone,Debug)]
pub enum Dimension {
    Nether = -1,
    Overworld = 0,
    End = 1,
}

#[derive(Copy,Clone,Debug)]
pub enum LevelType {
    Default,
    Flat,
    LargeBiomes,
    Amplified,
    Default1_1,
}

impl LevelType {
    pub fn as_str(&self) -> &'static str {
        match self {
            LevelType::Default => "default",
            LevelType::Flat => "flat",
            LevelType::LargeBiomes => "largeBiomes",
            LevelType::Amplified => "amplified",
            LevelType::Default1_1 => "default_1_1",
        }
    }
}

#[derive(Copy,Clone,Debug)]
pub enum Difficulty {
    Peaceful = 0,
    Easy = 1,
    Normal = 2,
    Hard = 3,
}

#[derive(Copy,Clone,Debug)]
pub enum ChatMode {
    Enabled = 0,
    CommandsOnly = 1,
    Hidden = 2,
}

impl ChatMode {
    pub fn from_i32(n: i32) -> Result<Self, Error> {
        Ok(match n {
            0 => ChatMode::Enabled,
            1 => ChatMode::CommandsOnly,
            2 => ChatMode::Hidden,
            _ => bail!("Invalid chat mode {}", n),
        })
    }
}

#[derive(EnumFlags,Copy,Clone,Debug)]
#[repr(u8)]
pub enum SkinPart {
    Cape = 0x01,
    Jacket = 0x02,
    LeftSleeve = 0x04,
    RightSleeve = 0x08,
    LeftPantsLeg = 0x10,
    RightPantsLeg = 0x20,
    Hat = 0x40,
}

#[derive(Copy,Clone,Debug)]
pub enum Hand {
    Left = 0,
    Right = 1,
}

impl Hand {
    pub fn from_i32(n: i32) -> Result<Self, Error> {
        Ok(match n {
            0 => Hand::Left,
            1 => Hand::Right,
            _ => bail!("Invalid hand {}", n),
        })
    }
}

#[derive(Debug,Copy,Clone)]
pub enum DigStatus {
    Start = 0,
    Cancel = 1,
    Finish = 2,
    DropStack = 3,
    DropItem = 4,
    ShootArrowOrFinishEating = 5,
    SwapItemInHand = 6,
}

impl DigStatus {
    pub fn from_i32(n: i32) -> Result<Self, Error> {
        Ok(match n {
            0 => DigStatus::Start,
            1 => DigStatus::Cancel,
            2 => DigStatus::Finish,
            3 => DigStatus::DropStack,
            4 => DigStatus::DropItem,
            5 => DigStatus::ShootArrowOrFinishEating,
            6 => DigStatus::SwapItemInHand,
            _ => bail!("Invalid dig status {}", n),
        })
    }
}

#[derive(Debug,Copy,Clone)]
pub enum Face {
    Bottom = 0,
    Top = 1,
    North = 2,
    South = 3,
    West = 4,
    East = 5,
}

impl Face {
    pub fn from_i8(n: i8) -> Result<Self, Error> {
        Ok(match n {
            0 => Face::Bottom,
            1 => Face::Top,
            2 => Face::North,
            3 => Face::South,
            4 => Face::West,
            5 => Face::East,
            _ => bail!("Invalid face {}", n),
        })
    }
}

#[derive(Debug,Copy,Clone)]
pub enum EntityAction {
    StartSneak = 0,
    StopSneak = 1,
    LeaveBead = 2,
    StartSprint = 3,
    StopSprint = 4,
    StartJumpWithHorse = 5,
    StopJumpWithHorse = 6,
    OpenHorseInventory = 7,
    StartFlyingWithElytra = 8,
}

impl EntityAction {
    pub fn from_i32(n: i32) -> Result<Self, Error> {
        Ok(match n {
            0 => EntityAction::StartSneak,
            1 => EntityAction::StopSneak,
            2 => EntityAction::LeaveBead,
            3 => EntityAction::StartSprint,
            4 => EntityAction::StopSprint,
            5 => EntityAction::StartJumpWithHorse,
            6 => EntityAction::StopJumpWithHorse,
            7 => EntityAction::OpenHorseInventory,
            8 => EntityAction::StartFlyingWithElytra,
            _ => bail!("Invalid entity action id"),
        })
    }
}
