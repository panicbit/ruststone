use failure::Error;
use std::io::{self, Read, Write};
use types::*;
use slog::Logger;

pub const MAX_PACKET_SIZE: i32 = 64 * 1024;

pub trait ReadPacketExt {
    fn receive<D: Deserialize>(&mut self, logger: &Logger) -> Result<D, Error>;
}

impl<R: Read> ReadPacketExt for R {
    fn receive<D: Deserialize>(&mut self, logger: &Logger) -> Result<D, Error> {
        let len = *VarInt::deserialize(self)?;
        trace!(logger, "Packet len: 0x{:02x}", len);

        if len > MAX_PACKET_SIZE {
            bail!("Maximum packet size exceeded");
        }

        let mut reader = self.take(len as u64);

        let id = *VarInt::deserialize(&mut reader)?;
        trace!(logger, "Packet id: 0x{:02x}", id);

        let packet = D::deserialize(&mut reader, id)?;

        if reader.limit() > 0 {
            warn!(logger, "{} unconsumed packet bytes", reader.limit());

            io::copy(&mut reader, &mut io::sink())?;
        }

        Ok(packet)
    }
}

pub trait WritePacketExt {
    fn send<S: Serialize>(&mut self, value: &S, logger: &Logger) -> Result<(), Error>;
}

impl<W: Write> WritePacketExt for W {
    fn send<S: Serialize>(&mut self, value: &S, logger: &Logger) -> Result<(), Error> {
        let mut buf = Vec::with_capacity(4 * 1024);

        value.serialize(&mut buf, logger)?;

        let id = VarInt(value.id());
        let len = id.serialized_len() as i32 + buf.len() as i32;
        let len = VarInt(len);

        len.serialize(self)?;
        id.serialize(self)?;
        self.write_all(&buf)?;

        Ok(())
    }
}

pub trait Deserialize: Sized {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error>;
}

pub trait Serialize {
    fn id(&self) -> i32;
    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error>;
    // fn serialized_size(&self) -> Option<usize>;
}
