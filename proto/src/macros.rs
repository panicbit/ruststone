
macro_rules! ensure_id {
    ($got:ident, $expected:path) => {
        ensure!($got == $expected, "Expected packet 0x{:02x} but got 0x{:02x}", $expected, $got);
    }
}
