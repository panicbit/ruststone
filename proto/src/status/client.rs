use std::io::{Read, Write};
use failure::Error;
use packet;
use types::*;
use base64::display::Base64Display;
use slog::Logger;

pub enum Packet {
    Response(Response),
    Pong(Pong),
}

#[derive(Serialize,Debug)]
pub struct Response {
    version: Version,
    players: Players,
    description: Chat,
    favicon: Option<String>,
}

impl Response {
    const ID: i32 = 0x00;

    pub fn new(players: Players) -> Self {
        let favicon = include_bytes!("../../../favicon2.png");
        let favicon = Base64Display::standard(favicon);
        let favicon = format!("data:image/png;base64,{}", favicon);

        Response {
            version: Version {
                name: ::VERSION,
                protocol: ::PROTOCOL,
            },
            players,
            description: Chat {
                text: "Hello from Rust".into(),
            },
            favicon: Some(favicon),
        }
    }
}

impl packet::Serialize for Response {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        let resp = ::json::to_string(self)?;
        trace!(logger, "Status response: {:#?}", self);

        let resp = Str::new(resp);

        resp.serialize(writer)?;

        Ok(())
    }
}

#[derive(Serialize,Debug)]
pub struct Version {
    name: &'static str,
    protocol: i32,
}

#[derive(Serialize,Debug)]
pub struct Players {
    max: u16,
    online: u16,
    sample: Vec<()>,
}

impl Players {
    pub fn new() -> Self {
        Players {
            max: 777,
            online: 42,
            sample: Vec::new(),
        }
    }
}

#[derive(Debug,Copy,Clone)]
pub struct Pong(pub i64);

impl Pong {
    const ID: i32 = 0x01;

    pub fn new(id: i64) -> Self {
        Pong(id)
    }
}

impl packet::Deserialize for Pong {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);
        let n = Long::deserialize(reader)?;
        Ok(Self::new(*n))
    }
}

impl packet::Serialize for Pong {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        Long(self.0).serialize(writer)
    }
}
