use std::io::{Read, Write};
use failure::Error;
use packet;
use types::*;
use base64::display::Base64Display;
use slog::Logger;

#[derive(Debug)]
pub enum Packet {
    Request(Request),
    Ping(Ping),
}

impl packet::Deserialize for Packet {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        Ok(match id {
            Request::ID => Packet::Request(Request::deserialize(reader, id)?),
            Ping::ID => Packet::Ping(Ping::deserialize(reader, id)?),
            _ => bail!("Unexpected status packet id {}", id),
        })
    }
}

#[derive(Debug)]
pub struct Request(());

impl Request {
    const ID: i32 = 0x00;

    pub fn new() -> Self {
        Request(())
    }
}

impl packet::Deserialize for Request {
    fn deserialize<R: Read>(_reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);
        Ok(Self::new())
    }
}

#[derive(Debug,Copy,Clone)]
pub struct Ping(pub i64);

impl Ping {
    const ID: i32 = 0x01;

    pub fn new(id: i64) -> Self {
        Ping(id)
    }
}

impl packet::Deserialize for Ping {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);
        let n = Long::deserialize(reader)?;
        Ok(Self::new(*n))
    }
}

impl packet::Serialize for Ping {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        Long(self.0).serialize(writer)
    }
}
