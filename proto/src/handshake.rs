use failure::Error;

pub mod server;

#[derive(Debug,Copy,Clone)]
pub enum NextState {
    Status,
    Login,
}

impl NextState {
    fn from_i32(n: i32) -> Result<Self, Error> {
        Ok(match n {
            1 => NextState::Status,
            2 => NextState::Login,
            _ => bail!("Unknown handshake next status {}", n),
        })
    }
}
