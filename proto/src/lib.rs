#![feature(non_modrs_mods)]

extern crate byteorder;
extern crate serde;
extern crate serde_json as json;
extern crate base64;
extern crate uuid;
extern crate enumflags;
#[macro_use] extern crate slog;
#[macro_use] extern crate failure;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate enumflags_derive;

pub const VERSION: &str = "1.12.2";
pub const PROTOCOL: i32 = 340;

#[macro_use]
pub mod macros;
pub mod types;
pub mod packet;
pub mod handshake;
pub mod status;
pub mod login;
pub mod play;
