use std::io::{Read,Write};
use failure::Error;
use uuid::Uuid;
use types::*;
use packet;
use slog::Logger;

pub enum Packet {
    Success,
}

pub struct Success<'a> {
    uuid: Uuid,
    username: Str<'a>,
}

impl<'a> Success<'a> {
    const ID: i32 = 0x02;

    pub fn new(uuid: Uuid, username: impl Into<Str<'a>>) -> Self {
        Self {
            uuid,
            username: username.into(),
        }
    }
    
    pub fn uuid(&self) -> &Uuid {
        &self.uuid
    }

    pub fn username(&'a self) -> &'a str {
        &self.username
    }
}

impl<'a> packet::Serialize for Success<'a> {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        let uuid = self.uuid.hyphenated().to_string();
        
        Str::new(uuid).serialize(writer)?;
        self.username.serialize(writer)
    }
}
