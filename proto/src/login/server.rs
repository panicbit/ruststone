use std::io::{Read,Write};
use failure::Error;
use types::*;
use packet;

#[derive(Debug)]
pub enum Packet<'a> {
    Start(Start<'a>),
}

impl<'a> packet::Deserialize for Packet<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        Ok(match id {
            Start::ID => Packet::Start(Start::deserialize(reader, id)?),
            _ => bail!("Unexpected login packet 0x{:02x}", id),
        })
    }
}

#[derive(Debug)]
pub struct Start<'a> {
    username: Str<'a>,
}

impl<'a> Start<'a> {
    const ID: i32 = 0x00;

    pub fn username(&'a self) -> &'a str {
        &self.username
    }
}

impl<'a> packet::Deserialize for Start<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            username: Str::deserialize(reader, 16)?,
        })
    }
}
