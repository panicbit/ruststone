use std::io::{Read, Write};
use failure::Error;
use packet;
use types::*;
use slog::Logger;
use std::borrow::Cow;
use enumflags::BitFlags;
use super::{ChatMode, SkinPart, Hand, Face, DigStatus};

#[derive(Debug)]
pub enum Packet<'a> {
    TeleportConfirm(TeleportConfirm),
    ClientSettings(ClientSettings<'a>),
    PluginMessage(PluginMessage<'a>),
    PlayerAbilities(PlayerAbilities),
    PlayerDigging(PlayerDigging),
    EntityAction(EntityAction),
    PlayerPositionAndLook(PlayerPositionAndLook),
    PlayerLook(PlayerLook),
    PlayerPosition(PlayerPosition),
    KeepAlive(KeepAlive),
}

impl<'a> packet::Deserialize for Packet<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        Ok(match id {
            TeleportConfirm::ID => Packet::TeleportConfirm(TeleportConfirm::deserialize(reader, id)?),
            ClientSettings::ID => Packet::ClientSettings(ClientSettings::deserialize(reader, id)?),
            PluginMessage::ID => Packet::PluginMessage(PluginMessage::deserialize(reader, id)?),
            PlayerAbilities::ID => Packet::PlayerAbilities(PlayerAbilities::deserialize(reader, id)?),
            PlayerDigging::ID => Packet::PlayerDigging(PlayerDigging::deserialize(reader, id)?),
            EntityAction::ID => Packet::EntityAction(EntityAction::deserialize(reader, id)?),
            PlayerPositionAndLook::ID => Packet::PlayerPositionAndLook(PlayerPositionAndLook::deserialize(reader, id)?),
            PlayerLook::ID => Packet::PlayerLook(PlayerLook::deserialize(reader, id)?),
            PlayerPosition::ID => Packet::PlayerPosition(PlayerPosition::deserialize(reader, id)?),
            KeepAlive::ID => Packet::KeepAlive(KeepAlive::deserialize(reader, id)?),
            _ => bail!("Unexpected play packet id 0x{:02x}", id),
        })
    }
}

#[derive(Debug)]
pub struct TeleportConfirm {
    pub teleport_id: i32,
}

impl TeleportConfirm {
    pub const ID: i32 = 0x00;
}

impl packet::Deserialize for TeleportConfirm {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            teleport_id: VarInt::deserialize(reader)?.0,
        })
    }
}

#[derive(Debug)]
pub struct PluginMessage<'a> {
    pub channel: Cow<'a, str>,
    pub data: Cow<'a, [u8]>,
}

impl<'a> PluginMessage<'a> {
    pub const ID: i32 = 0x09;
}

impl<'a> packet::Deserialize for PluginMessage<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            channel: Str::deserialize(reader, 20)?.0,
            data: {
                let mut data = Vec::new();
                reader.read_to_end(&mut data)?;
                data.into()
            }
        })
    }
}

#[derive(Debug)]
pub struct PlayerAbilities {
    pub flags: u8,
    pub flying_speed: f32,
    pub walking_speed: f32,
}

impl PlayerAbilities {
    pub const ID: i32 = 0x13;
}

impl packet::Deserialize for PlayerAbilities {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            flags: UByte::deserialize(reader)?.0,
            flying_speed: Float::deserialize(reader)?.0,
            walking_speed: Float::deserialize(reader)?.0,
        })
    }
}

#[derive(Debug)]
pub struct PlayerDigging {
    pub status: DigStatus,
    pub location: Position,
    pub face: Face,
}

impl PlayerDigging {
    pub const ID: i32 = 0x14;
}

impl packet::Deserialize for PlayerDigging {
    fn deserialize<R:Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            status: DigStatus::from_i32(VarInt::deserialize(reader)?.0)?,
            location: Position::deserialize(reader)?,
            face: Face::from_i8(Byte::deserialize(reader)?.0)?,
        })
    }
}

#[derive(Debug)]
pub struct EntityAction {
    pub entity_id: i32,
    pub action: super::EntityAction,
    pub jump_boost: u8,
}

impl EntityAction {
    pub const ID: i32 = 0x15;
}

impl packet::Deserialize for EntityAction {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            entity_id: VarInt::deserialize(reader)?.0,
            action: super::EntityAction::from_i32(VarInt::deserialize(reader)?.0)?,
            jump_boost: VarInt::deserialize(reader)?.0 as u8,
        })
    }
}

#[derive(Debug)]
pub struct ClientSettings<'a> {
    pub locale: Cow<'a, str>,
    pub view_distance: i8,
    pub chat_mode: ChatMode,
    pub chat_colors: bool,
    pub displayed_skin_parts: BitFlags<SkinPart>,
    pub main_hand: Hand,
}

impl<'a> ClientSettings<'a> {
    pub const ID: i32 = 0x04;
}

impl<'a> packet::Deserialize for ClientSettings<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            locale: Str::deserialize(reader, 16)?.0,
            view_distance: Byte::deserialize(reader)?.0,
            chat_mode: ChatMode::from_i32(VarInt::deserialize(reader)?.0)?,
            chat_colors: Boolean::deserialize(reader)?.0,
            displayed_skin_parts: BitFlags::from_bits_truncate(UByte::deserialize(reader)?.0),
            main_hand: Hand::from_i32(VarInt::deserialize(reader)?.0)?
        })
    }
}

#[derive(Debug)]
pub struct PlayerPositionAndLook {
    pub x: f64,
    pub feet_y: f64,
    pub z: f64,
    pub yaw: f32,
    pub pitch: f32,
    pub on_ground: bool,
}

impl PlayerPositionAndLook {
    pub const ID: i32 = 0x0e;
}

impl packet::Deserialize for PlayerPositionAndLook {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            x: Double::deserialize(reader)?.0,
            feet_y: Double::deserialize(reader)?.0,
            z: Double::deserialize(reader)?.0,
            yaw: Float::deserialize(reader)?.0,
            pitch: Float::deserialize(reader)?.0,
            on_ground: Boolean::deserialize(reader)?.0,
        })
    }
}

#[derive(Debug)]
pub struct PlayerLook {
    yaw: f32,
    pitch: f32,
    on_ground: bool,
}

impl PlayerLook {
    pub const ID: i32 = 0x0f;
}

impl packet::Deserialize for PlayerLook {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            yaw: Float::deserialize(reader)?.0,
            pitch: Float::deserialize(reader)?.0,
            on_ground: Boolean::deserialize(reader)?.0,
        })
    }
}

#[derive(Debug)]
pub struct PlayerPosition {
    pub x: f64,
    pub feet_y: f64,
    pub z: f64,
    pub on_ground: bool,
}

impl PlayerPosition {
    pub const ID: i32 = 0x0d;
}

impl packet::Deserialize for PlayerPosition {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(PlayerPosition {
            x: Double::deserialize(reader)?.0,
            feet_y: Double::deserialize(reader)?.0,
            z: Double::deserialize(reader)?.0,
            on_ground: Boolean::deserialize(reader)?.0,
        })
    }
}

#[derive(Debug)]
pub struct KeepAlive {
    pub id: i64,
}

impl KeepAlive {
    pub const ID: i32 = 0x0B;
}

impl packet::Deserialize for KeepAlive {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            id: Long::deserialize(reader)?.0,
        })
    }
}
