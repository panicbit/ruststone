use std::io::{Read, Write};
use failure::Error;
use packet;
use types::*;
use slog::Logger;
use std::borrow::Cow;
use super::{GameMode,Dimension,Difficulty,LevelType};

#[derive(Debug)]
pub enum Packet<'a> {
    PluginMessage(PluginMessage<'a>),
    KeepAlive(KeepAlive),
    ChunkData(ChunkData<'a>),
    JoinGame(JoinGame),
    PlayerPositionAndLook(PlayerPositionAndLook),
    SpawnPosition(SpawnPosition),
}

impl<'a> packet::Serialize for Packet<'a> {
    fn id(&self) -> i32 {
        match self {
            Packet::PluginMessage(v) => v.id(),
            Packet::KeepAlive(v) => v.id(),
            Packet::ChunkData(v) => v.id(),
            Packet::JoinGame(v) => v.id(),
            Packet::PlayerPositionAndLook(v) => v.id(),
            Packet::SpawnPosition(v) => v.id(),
        }
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        match self {
            Packet::PluginMessage(v) => v.serialize(writer, logger),
            Packet::KeepAlive(v) => v.serialize(writer, logger),
            Packet::ChunkData(v) => v.serialize(writer, logger),
            Packet::JoinGame(v) => v.serialize(writer, logger),
            Packet::PlayerPositionAndLook(v) => v.serialize(writer, logger),
            Packet::SpawnPosition(v) => v.serialize(writer, logger),
        }
    }
}

#[derive(Debug)]
pub struct PluginMessage<'a> {
    pub channel: Cow<'a, str>,
    pub data: Cow<'a, [u8]>,
}

impl<'a> PluginMessage<'a> {
    pub const ID: i32 = 0x18;
}

impl<'a> packet::Serialize for PluginMessage<'a> {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        if self.channel.chars().count() > 20 {
            warn!(logger, "Length of plugin channel name '{}' exceeds limit (20)", self.channel);
        }

        Str::new(&*self.channel).serialize(writer)?;
        writer.write_all(&self.data)?;

        Ok(())
    }
}

impl<'a> From<PluginMessage<'a>> for Packet<'a> {
    fn from(v: PluginMessage<'a>) -> Self {
        Packet::PluginMessage(v)
    }
}

#[derive(Debug)]
pub struct KeepAlive {
    pub id: i64
}

impl KeepAlive {
    pub const ID: i32 = 0x1F;
}

impl packet::Serialize for KeepAlive {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        Long(self.id).serialize(writer)?;

        Ok(())
    }
}

impl<'a> From<KeepAlive> for Packet<'a> {
    fn from(v: KeepAlive) -> Self {
        Packet::KeepAlive(v)
    }
}

#[derive(Debug)]
pub struct ChunkData<'a> {
    pub chunk_x: i32,
    pub chunk_z: i32,
    pub ground_up_continuous: bool,
    pub primary_bit_mask: u16,
    pub data: Cow<'a, [u8]>,
    pub block_entities: Cow<'a, [u8]>,
}

impl<'a> ChunkData<'a> {
    pub const ID: i32 = 0x20;
}

impl<'a> packet::Serialize for ChunkData<'a> {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        warn!(logger, "ChunkData serialize not yet fully implemented");

        Int(self.chunk_x).serialize(writer)?;
        Int(self.chunk_z).serialize(writer)?;
        Boolean(self.ground_up_continuous).serialize(writer)?;
        VarInt(self.primary_bit_mask as i32).serialize(writer)?;
        VarInt(self.data.len() as i32).serialize(writer)?;
        writer.write_all(&self.data)?;
        VarInt(self.block_entities.len() as i32).serialize(writer)?;
        writer.write_all(&self.block_entities)?;

        Ok(())
    }
}

impl<'a> From<ChunkData<'a>> for Packet<'a> {
    fn from(v: ChunkData<'a>) -> Self {
        Packet::ChunkData(v)
    }
}

#[derive(Debug)]
pub struct JoinGame {
    pub entity_id: i32,
    pub game_mode: GameMode,
    pub is_hardcore: bool,
    pub dimension: Dimension,
    pub difficulty: Difficulty,
    pub level_type: LevelType,
    pub reduced_debug_info: bool,
}

impl JoinGame {
    pub const ID: i32 = 0x23;
}

impl packet::Serialize for JoinGame {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        let game_mode =
              self.game_mode as u8
            | (self.is_hardcore as u8) << 3;

        Int(self.entity_id).serialize(writer)?;
        UByte(game_mode).serialize(writer)?;
        Int(self.dimension as i32).serialize(writer)?;
        UByte(self.difficulty as u8).serialize(writer)?;
        // Was MaxPlayers, is ignored now
        UByte(0).serialize(writer)?;
        Str::new(self.level_type.as_str()).serialize(writer)?;
        Boolean(self.reduced_debug_info).serialize(writer)?;

        Ok(())
    }
}

impl<'a> From<JoinGame> for Packet<'a> {
    fn from(v: JoinGame) -> Self {
        Packet::JoinGame(v)
    }
}

#[derive(Debug)]
pub struct PlayerPositionAndLook {
    pub x: f64,
    pub y: f64,
    pub z: f64,
    pub yaw: f32,
    pub pitch: f32,
    pub is_x_relative: bool,
    pub is_y_relative: bool,
    pub is_z_relative: bool,
    pub is_yaw_relative: bool,
    pub is_pitch_relative: bool,
    pub teleport_id: i32,
}

impl PlayerPositionAndLook {
    pub const ID: i32 = 0x2F;
}

impl Default for PlayerPositionAndLook {
    fn default() -> Self {
        Self {
            x: 0.,
            y: 64.,
            z: 0.,
            yaw: 0.,
            pitch: 0.,
            is_x_relative: false,
            is_y_relative: false,
            is_z_relative: false,
            is_yaw_relative: false,
            is_pitch_relative: false,
            teleport_id: 0,
        }
    }
}

impl packet::Serialize for PlayerPositionAndLook {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, _logger: &Logger) -> Result<(), Error> {
        let flag =
              (self.is_x_relative as u8) << 0
            | (self.is_y_relative as u8) << 1
            | (self.is_z_relative as u8) << 2
            | (self.is_pitch_relative as u8) << 3
            | (self.is_yaw_relative as u8) << 4;
        
        Double(self.x).serialize(writer)?;
        Double(self.y).serialize(writer)?;
        Double(self.z).serialize(writer)?;
        Float(self.yaw).serialize(writer)?;
        Float(self.pitch).serialize(writer)?;
        Byte(flag as i8).serialize(writer)?;
        VarInt(self.teleport_id).serialize(writer)?;

        Ok(())
    }
}

impl<'a> From<PlayerPositionAndLook> for Packet<'a> {
    fn from(v: PlayerPositionAndLook) -> Self {
        Packet::PlayerPositionAndLook(v)
    }
}

#[derive(Debug)]
pub struct SpawnPosition {
    x: i32,
    y: i32,
    z: i32,
}

impl SpawnPosition {
    pub const ID: i32 = 0x46;

    pub fn new(x: i32, y: i32, z: i32) -> SpawnPosition {
        Self { x, y, z }
    }
}

impl packet::Serialize for SpawnPosition {
    fn id(&self) -> i32 {
        Self::ID
    }

    fn serialize<W: Write>(&self, writer: &mut W, logger: &Logger) -> Result<(), Error> {
        let position = Position {
            x: self.x,
            y: self.y,
            z: self.z,
        };
        position.serialize(writer)
    }
}

impl<'a> From<SpawnPosition> for Packet<'a> {
    fn from(v: SpawnPosition) -> Self {
        Packet::SpawnPosition(v)
    }
}
