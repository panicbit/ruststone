use std::io::{Read,Write};
use std::ops::{Deref, DerefMut};
use byteorder::{ReadBytesExt, WriteBytesExt};
use failure::Error;

macro_rules! varint {
    ($struct:ident, $int:ty, $uint:ty, $max_bytes:expr) => {
        #[derive(PartialEq,Eq,Debug)]
        pub struct $struct(pub $int);

        impl $struct {
            pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
                let mut num_read = 0;
                let mut result = 0;

                loop {
                    let read = reader.read_u8()?;
                    let value = (read & 0b01111111) as $int;

                    result |= value.wrapping_shl(7 * num_read);

                    num_read += 1;

                    if num_read > $max_bytes {
                        bail!("varint too big");
                    }

                    if (read & 0b10000000) == 0 {
                        break;
                    }

                }

                Ok($struct(result))
            }

            pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
                let mut value = self.0 as $uint;
                
                loop {
                    let mut temp = (value & 0b01111111) as u8;

                    value >>= 7;

                    if value != 0 {
                        temp |= 0b10000000;
                    }

                    writer.write_u8(temp)?;

                    if value == 0 {
                        break;
                    }
                }

                Ok(())
            }

            pub fn serialized_len(&self) -> u8 {
                let len = ::std::mem::size_of::<$int>() as u32 * 8 - self.0.leading_zeros();
                let len = (len as f32 / 7.).ceil() as usize;
                let len = ::std::cmp::max(1, len);

                len as u8
            }
        }

        impl Deref for $struct {
            type Target = $int;

            fn deref(&self) -> &Self::Target {
                &self.0
            }
        }

        impl DerefMut for $struct {
            fn deref_mut(&mut self) -> &mut Self::Target {
                &mut self.0
            }
        }
    }
}

varint!(VarInt, i32, u32, 5);
varint!(VarLong, i64, u64, 10);

#[cfg(test)]
mod tests {
    use super::*;

    const INT_SAMPLES: &[(i32, &[u8])] = &[
        (0, &[0]),
        (1, &[1]),
        (127, &[127]),
        (128, &[128, 1]),
        (255, &[255, 1]),
        (2147483647, &[255, 255, 255, 255, 7]),
        (-1, &[255, 255, 255, 255, 15]),
        (-2147483648, &[128, 128, 128, 128, 8]),
    ];

    const LONG_SAMPLES: &[(i64, &[u8])] = &[
        (0, &[0]),
        (1, &[1]),
        (127, &[127]),
        (128, &[128, 1]),
        (255, &[255, 1]),
        (2147483647, &[255, 255, 255, 255, 7]),
        (9223372036854775807, &[255, 255, 255, 255, 255, 255, 255, 255, 127]),
        (-1, &[255, 255, 255, 255, 255, 255, 255, 255, 255, 1]),
        (-2147483648, &[128, 128, 128, 128, 248, 255, 255, 255, 255, 1]),
        (-9223372036854775808, &[128, 128, 128, 128, 128, 128, 128, 128, 128, 1]),
    ];

    #[test]
    fn varint_deserialize() {
        for &(expected, mut input) in INT_SAMPLES {
            eprintln!("Testing {}", expected);
            assert_eq!(*VarInt::deserialize(&mut input).unwrap(), expected);
        }
    }

    #[test]
    fn varlong_deserialize() {
        for &(expected, mut input) in LONG_SAMPLES {
            eprintln!("Testing {}", expected);
            assert_eq!(*VarLong::deserialize(&mut input).unwrap(), expected);
        }
    }

    #[test]
    fn varint_serialize() {
        for &(value, bytes) in INT_SAMPLES {
            let mut buf = Vec::new();

            eprintln!("Testing {}", value);
            
            VarInt(value).serialize(&mut buf).unwrap();
            assert_eq!(&*buf, bytes);
        }
    }

    #[test]
    fn varlong_serialize() {
        for &(value, bytes) in LONG_SAMPLES {
            let mut buf = Vec::new();

            eprintln!("Testing {}", value);
            
            VarLong(value).serialize(&mut buf).unwrap();
            assert_eq!(&*buf, bytes);
        }
    }

    #[test]
    fn varint_serialized_len() {
        for &(value, bytes) in INT_SAMPLES {
            eprintln!("Testing {}", value);
            
            assert_eq!(VarInt(value).serialized_len() as usize, bytes.len());
        }
    }

    #[test]
    fn varlong_serialized_len() {
        for &(value, bytes) in LONG_SAMPLES {
            eprintln!("Testing {}", value);
            
            assert_eq!(VarLong(value).serialized_len() as usize, bytes.len());
        }
    }
}
