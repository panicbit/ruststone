use std::io::{Read,Write};
use byteorder::{BE,ReadBytesExt,WriteBytesExt};
use failure::Error;
use std::ops::{Deref,DerefMut};

mod varint;
pub use self::varint::{VarInt, VarLong};

mod string;
pub use self::string::Str;

pub struct Boolean(pub bool);

impl Boolean {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_u8()?;
        Ok(Boolean(n != 0))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_u8(self.0 as u8)?;
        Ok(())
    }
}

impl Deref for Boolean {
    type Target = bool;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Boolean {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Byte(pub i8);

impl Byte {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_i8()?;
        Ok(Byte(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_i8(self.0).map_err(Error::from)
    }
}

pub struct UByte(pub u8);

impl UByte {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_u8()?;
        Ok(UByte(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_u8(self.0)?;
        Ok(())
    }
}

impl Deref for UByte {
    type Target = u8;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for UByte {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Short(i16);

impl Short {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_i16::<BE>()?;
        Ok(Short(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_i16::<BE>(self.0)?;
        Ok(())
    }
}

impl Deref for Short {
    type Target = i16;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Short {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct UShort(u16);

impl UShort {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_u16::<BE>()?;
        Ok(UShort(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_u16::<BE>(self.0)?;
        Ok(())
    }
}

impl Deref for UShort {
    type Target = u16;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for UShort {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl UShort {
    pub fn read<R: Read>(reader: &mut R) -> Result<u16, Error> {
        let n = reader.read_u16::<BE>()?;
        Ok(n)
    }
}

pub struct Int(pub i32);

impl Int {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_i32::<BE>()?;
        Ok(Int(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_i32::<BE>(self.0)?;
        Ok(())
    }
}

impl Deref for Int {
    type Target = i32;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Int {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Long(pub i64);

impl Long {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = reader.read_i64::<BE>()?;
        Ok(Long(n))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_i64::<BE>(self.0)?;
        Ok(())
    }
}

impl Deref for Long {
    type Target = i64;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for Long {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub struct Float(pub f32);

impl Float {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        reader.read_f32::<BE>().map(Float).map_err(Error::from)
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_f32::<BE>(self.0).map_err(Error::from)
    }
}

pub struct Double(pub f64);

impl Double {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        reader.read_f64::<BE>().map(Double).map_err(Error::from)
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        writer.write_f64::<BE>(self.0).map_err(Error::from)
    }
}

#[derive(Serialize,Debug)]
pub struct Chat {
    pub text: String,
}

pub struct Identifier(String);
pub struct EntityMetadata(());
pub struct Slot(());
pub struct NBTTag(());

#[derive(Debug)]
pub struct Position {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Position {
    pub fn deserialize<R: Read>(reader: &mut R) -> Result<Self, Error> {
        let n = Long::deserialize(reader)?.0 as u64;

        let mut pos = Self {
            x: (n >> 38) as i32,
            y: ((n >> 26) & 0xFFF) as i32,
            z: ((n << 38) >> 38) as i32,
        };

        if pos.x >= 1 << 25 {
            pos.x -= 1 << 26;
        }

        if pos.y >= 1 << 11 {
            pos.y -= 1 << 12;
        }

        if pos.z >= 1 << 25 {
            pos.z -= 1 << 26;
        }

        Ok(pos)
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        let position: u64 =
              ((self.x as u64 & 0x3FFFFFF) << 38)
            | ((self.y as u64 & 0xFFF    ) << 26)
            | ( self.z as u64 & 0x3FFFFFF);
        Long(position as i64).serialize(writer)
    }
}

pub struct Angle(u8);
pub struct UUID([u8; 16]);
