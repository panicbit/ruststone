use std::io::{Read,Write};
use std::cmp;
use std::borrow::Cow;
use failure::Error;
use types::VarInt;
use std::ops;

#[derive(Debug)]
pub struct Str<'a>(pub Cow<'a, str>);

impl<'a> Str<'a> {
    const MAX_CHARS: usize = 32767;

    pub fn new(s: impl Into<Cow<'a, str>>) -> Self {
        Str(s.into())
    }

    pub fn deserialize<R: Read>(reader: &mut R, max_chars: usize) -> Result<Str<'a>, Error> {
        let max_chars = cmp::min(max_chars, Self::MAX_CHARS);
        let max_bytes = 4 * max_chars;
        let len = *VarInt::deserialize(reader)? as usize;
        let mut buf = String::with_capacity(len);

        ensure!(len < max_bytes, "String len ({}) exceeds byte limit ({})", len, max_bytes);

        let n_read = reader.take(len as u64).read_to_string(&mut buf)?;

        ensure!(n_read == len, "Unexpected end of string");

        Ok(Str(Cow::from(buf)))
    }

    pub fn serialize<W: Write>(&self, writer: &mut W) -> Result<(), Error> {
        if self.0.chars().count() > Self::MAX_CHARS {
            bail!("Max chars exceeded");
        }

        let len = VarInt(self.0.len() as i32);
        len.serialize(writer)?;

        writer.write_all(self.0.as_bytes())?;

        Ok(())
    }
}

impl<'a, T: Into<Cow<'a, str>>> From<T> for Str<'a> {
    fn from(s: T) -> Self {
        Self::new(s)
    }
}

impl<'a> ops::Deref for Str<'a> {
    type Target = str;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
