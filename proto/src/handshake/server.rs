use std::io::Read;
use failure::Error;
use types::*;
use packet;

use super::NextState;

pub enum Handshake<'a> {
    Init(Init<'a>),
}

impl<'a> packet::Deserialize for Handshake<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        Ok(match id {
            Init::ID => Handshake::Init(Init::deserialize(reader, id)?),
            _ => bail!("Unexpected handshake packet 0x{:02x}"),
        })
    }
}

#[derive(Debug)]
pub struct Init<'a> {
    protocol_version: VarInt,
    server_address: Str<'a>,
    server_port: u16,
    next_state: NextState,
}

impl<'a> Init<'a> {
    const ID: i32 = 0x00;

    pub fn protocol_version(&self) -> i32 {
        *self.protocol_version
    }

    pub fn server_address(&self) -> &str {
        &self.server_address
    }

    pub fn server_port(&self) -> u16 {
        self.server_port
    }

    pub fn next_state(&self) -> NextState {
        self.next_state
    }
}

impl<'a> packet::Deserialize for Init<'a> {
    fn deserialize<R: Read>(reader: &mut R, id: i32) -> Result<Self, Error> {
        ensure_id!(id, Self::ID);

        Ok(Self {
            protocol_version: VarInt::deserialize(reader)?,
            server_address: Str::deserialize(reader, 255)?,
            server_port: UShort::read(reader)?,
            next_state: NextState::from_i32(*VarInt::deserialize(reader)?)?,
        })
    }
}
