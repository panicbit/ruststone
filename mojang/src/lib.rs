extern crate failure;
extern crate reqwest;
extern crate chrono;
extern crate uuid;
extern crate serde;
extern crate percent_encoding;
#[macro_use] extern crate serde_derive;

pub mod minecraft;
