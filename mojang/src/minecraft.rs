use failure::Error;
use chrono::{DateTime,Utc};
use uuid::Uuid;
use percent_encoding::{utf8_percent_encode, PATH_SEGMENT_ENCODE_SET};
use reqwest;

#[derive(Deserialize,Debug)]
pub struct Profile {
    id: Uuid,
    name: String,
    #[serde(default)]
    legacy: bool,
    #[serde(default)]
    demo: bool,
}

impl Profile {
    pub fn id(&self) -> &Uuid {
        &self.id
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn is_legacy(&self) -> bool {
        self.legacy
    }

    pub fn is_demo(&self) -> bool {
        self.demo
    }
}

pub fn get_profile(username: &str) -> Result<Profile, Error> {
    get_profile_at(username, None)
}

pub fn get_profile_at(username: &str, at: Option<DateTime<Utc>>) -> Result<Profile, Error> {
    let mut url = format!("https://api.mojang.com/users/profiles/minecraft/{username}",
        username = utf8_percent_encode(username, PATH_SEGMENT_ENCODE_SET),
    );

    if let Some(at) = at {
        use std::fmt::Write;
        write!(url, "?at={}", at.timestamp())?;
    }

    let profile = reqwest::get(&url)?
        .error_for_status()?
        .json()?;

    Ok(profile)
}
