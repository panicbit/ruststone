extern crate ruststone_proto as proto;
extern crate ruststone_mojang as mojang;
extern crate byteorder;
extern crate failure;
#[macro_use] extern crate slog;

use slog::Logger;
use std::net::{TcpStream, Shutdown};
use std::thread;
use std::sync::mpsc::channel;
use failure::Error;
use proto::packet::{ReadPacketExt, WritePacketExt};
use proto::{handshake, status, login, play};
use std::time::{SystemTime, Duration, UNIX_EPOCH};

pub struct Session {
    logger: Logger,
    conn: TcpStream,
}

impl Session {
    pub fn new(conn: TcpStream, logger: Logger) -> Result<Self, Error> {
        Ok(Self {
            logger,
            conn,
        })
    }

    pub fn run(mut self) -> Result<(), Error> {
        let res = || -> Result<_, Error>{
            match self.handle_handshake()? {
                handshake::NextState::Status => self.handle_status(),
                handshake::NextState::Login => {
                    let username = self.handle_login()?;
                    self.logger = self.logger.new(o!("username" => username));
                    self.handle_play()
                }
            }
        }();

        if res.is_err() {
            self.conn.shutdown(Shutdown::Both).ok();
        }

        res
    }

    fn handle_handshake(&mut self) -> Result<handshake::NextState, Error> {
        use handshake::server::Handshake;

        trace!(self.logger, "Handling handshake");
        let ref logger = self.logger.new(o!("state" => "handshake"));

        match self.conn.receive::<Handshake>(logger)? {
            Handshake::Init(init) => {
                debug!(logger, "{:#?}", init);

                Ok(init.next_state())
            }
        }
    }

    fn handle_status(&mut self) -> Result<(), Error> {
        use status::{server, client};

        trace!(self.logger, "Handling status");
        let ref logger = self.logger.new(o!("state" => "status"));

        loop {
            let packet = self.conn.receive::<server::Packet>(logger)?;
            trace!(logger, "{:#?}", packet);

            match packet {
                server::Packet::Request(_) => {
                    let players = client::Players::new();
                    let response = client::Response::new(players);
                    self.conn.send(&response, logger)?;
                },
                server::Packet::Ping(server::Ping(n)) => {
                    self.conn.send(&client::Pong(n), logger)?;
                }
            }
        }
    }

    fn handle_login(&mut self) -> Result<String, Error> {
        use login::{server, client};

        trace!(self.logger, "Handling login");
        let ref logger = self.logger.new(o!("state" => "login"));

        let start = self.conn.receive::<server::Start>(logger)?;
        trace!(logger, "{:#?}", start);

        let profile = mojang::minecraft::get_profile(start.username())?;
        trace!(logger, "{:#?}", profile);

        let success = client::Success::new(profile.id().clone(), profile.name());
        self.conn.send(&success, logger)?;

        Ok(profile.name().to_owned())
    }


    fn handle_play(&mut self) -> Result<(), Error> {
        use play::{server, client};

        trace!(self.logger, "Handling play");
        let ref logger = self.logger.new(o!("state" => "play"));

        // Spawn thread for sending packets
        let sender = {
            let (tx, rx) = channel::<client::Packet>();
            let logger = logger.clone();
            let mut conn = self.conn.try_clone()?;

            thread::spawn(move || {
                for packet in rx {
                    if conn.send(&packet, &logger).is_err() {
                        break;
                    }
                }

                trace!(logger, "Stopping sender loop");
            });

            tx
        };

        trace!(logger, "Sending join game");
        sender.send(client::JoinGame {
            entity_id: 42,
            game_mode: play::GameMode::Creative,
            is_hardcore: false,
            dimension: play::Dimension::Overworld,
            difficulty: play::Difficulty::Peaceful,
            level_type: play::LevelType::Flat,
            reduced_debug_info: false,
        }.into())?;

        trace!(logger, "Sending server brand");
        sender.send(client::PluginMessage {
            channel: "MC|Brand".into(),
            data: {
                let mut data = Vec::new();
                proto::types::Str::new("ruststone").serialize(&mut data)?;
                data.into()
            },
        }.into())?;

        trace!(logger, "Sending spawn position");
        sender.send(client::SpawnPosition::new(0, 64, 0).into())?;

        trace!(logger, "Sending player position and look");
        sender.send(client::PlayerPositionAndLook::default().into())?;

        trace!(logger, "Sending demo chunk");
        sender.send(client::ChunkData {
            chunk_x: 0,
            chunk_z: 0,
            ground_up_continuous: true,
            primary_bit_mask: 0,
            data: std::borrow::Cow::Borrowed(&[]),
            block_entities: std::borrow::Cow::Borrowed(&[]),
        }.into())?;

        {
            let sender = sender.clone();
            let logger = logger.clone();

            thread::spawn(move || {
                trace!(logger, "Starting ping loop");

                loop {
                    let ts = match SystemTime::now().duration_since(UNIX_EPOCH) {
                        Ok(ts) => {
                            1_000 * ts.as_secs() as i64 + ts.subsec_millis() as i64
                        },
                        Err(e) => {
                            warn!(logger, "Unable to get timestamp for ping {}", e);
                            thread::sleep(Duration::from_secs(1));
                            continue;
                        },
                    };

                    if sender.send(client::KeepAlive { id: ts }.into()).is_err() {
                        break;
                    }

                    thread::sleep(Duration::from_secs(15));
                }

                trace!(logger, "Stopping ping loop");
            });
        }

        loop {
            let packet = self.conn.receive::<server::Packet>(logger)?;
            trace!(logger, "Received packet: {:#?}", packet);
        }
    }
}
