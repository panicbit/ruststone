extern crate ruststone;
extern crate failure;
extern crate sloggers;
#[macro_use] extern crate slog;

use std::net::{TcpListener, TcpStream};
use std::thread;
use failure::Error;
use sloggers::Build;
use sloggers::terminal::TerminalLoggerBuilder;
use sloggers::types::{Severity,Format};
use slog::Logger;

use ruststone::Session;

fn main() -> Result<(), Error> {
    let logger = TerminalLoggerBuilder::new()
        .level(Severity::Trace)
        .format(Format::Compact)
        .build()
        .unwrap();
    let host = "127.0.0.1";
    let port = 25565;
    let listener = TcpListener::bind((host, port))?;

    info!(logger, "Listening"; "host" => host, "port" => port);

    for stream in listener.incoming() {
        let stream = stream?;
        let addr = stream.peer_addr()?;

        let logger = logger.new(o!(
            "client_addr" => %addr,
        ));

        info!(logger, "Client connected!");

        thread::spawn(move ||  {
            if let Err(e) = handle_client(stream, logger.clone()) {
                error!(logger, "Client error: {}", e);
            }

            info!(logger, "Client disconnected!");
        });
    }

    Ok(())
}

fn handle_client(stream: TcpStream, logger: Logger) -> Result<(), Error> {
    Session::new(stream, logger)?.run()
}
